using System;

namespace TccBack.Domain.Entities
{
	public class Payment : Entity
	{
		public Payment()
		{
			
		}
		
		public Payment(string id, string companyId, decimal credits, string cieloPaymentId)
		{
			Id = id;
			CompanyId = companyId;
			Credits = credits;
			CieloPaymentId = cieloPaymentId;
			CreatedAt = DateTime.Now;
		}

		public string CompanyId { get; set; }
		public decimal Credits { get; set; }
		public string CieloPaymentId { get; set; }
	}
}