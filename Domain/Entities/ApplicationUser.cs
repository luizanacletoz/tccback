using System;
using System.Linq;
using Raven.Identity;

namespace TccBack.Domain.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }

        public Guid GuidId()
        {
            return Guid.Parse(Id);
        }

        public bool IsAdmin()
        {
            return Roles.Contains("administrator");
        }
        
        protected ApplicationUser() 
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}