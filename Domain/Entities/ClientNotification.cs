using System;

namespace TccBack.Domain.Entities
{
	public class ClientNotification : Notification
	{
		public string ClientId { get; set; }
		public string BudgetId { get; set; }

		public ClientNotification()
		{
			
		}
		
		public ClientNotification(
			string title, 
			string message, 
			string clientId, 
			string budgetId) : base(title, message)
		{
			ClientId = clientId;
			BudgetId = budgetId;
		}
		
		public ClientNotification(
			string title, 
			string message, 
			string clientId) : base(title, message)
		{
			ClientId = clientId;
		}
	}
}