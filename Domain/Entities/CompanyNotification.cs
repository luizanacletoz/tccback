namespace TccBack.Domain.Entities
{
	public class CompanyNotification : Notification
	{
		public string CompanyId { get; set; }
		public string BudgetId { get; set; }
		public CompanyNotification()
		{
			
		}
		
		public CompanyNotification(
			string title, 
			string message, 
			string companyId,
			string budgetId) : base(title, message)
		{
			CompanyId = companyId;
			BudgetId = budgetId;
		}
	}
}