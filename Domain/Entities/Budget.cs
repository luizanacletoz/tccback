using System;
using System.Collections.Generic;
using System.Linq;
using Raven.Client.Documents.Linq;
using TccBack.Domain.DenormalizedReferences.Classes;
using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.Entities
{
	public class Budget : Entity
	{
		public Budget()
		{
			IsOpen = true;
			Items = new List<Item>();
			Companies = new List<DenormalizedCompany<Company>>();
			SelectedCompany = null;
		}
		
		public DenormalizedReference<Client> Client { get; set; }
		
		public bool IsOpen { get; set; }
		
		public decimal BestProposal { get; set; }
		
		public Company SelectedCompany { get; set; }
		
		public IList<Item> Items { get; set;  }
		public IList<BudgetResponse> Responses { get; set; }
		public IList<DenormalizedCompany<Company>> Companies { get; set; }
		
		public void AddCompany(Company company)
		{
			Companies.Add(company);
		}

		public void AddCompanies(IList<Company> companies)
		{
			foreach (var company in companies)
			{
				this.Companies.Add(company);
			}
		}

		public void AddItem(Item item)
		{
			Items.Add(item);
		}

		public bool CompanyInBudget(Company company)
		{
			return Companies.SingleOrDefault(x => x.Id.Equals(company.Id)) != null;
		}

		public BudgetResponse CreateResponse(Company company, decimal value, IList<ResponseItem> responseItems)
		{
			if (Responses == null)
				Responses = new List<BudgetResponse>();
			
			var response = new BudgetResponse(company, value, responseItems);
			Responses.Add(response);
	
			return response;
		}
	}
}