using System;
using TccBack.Domain.DenormalizedReferences.Interfaces;
using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.Entities
{
	public class Company : ApplicationUser, IDenormalizedCompany
	{
		public string Cnpj { get; set; }
		public string ImageUrl { get; set; }
		public decimal Credits { get; set; }
		public Address Address { get; set; }
		public double Lat { get; set; }
		public double Long { get; set; }
		public bool IsPreRegister { get; set; }
		
		
		public void AddCredits(decimal value)
		{
			Credits += value;
		}

		public void RemoveCredits(decimal value)
		{
			Credits -= value;
		}
	}
}