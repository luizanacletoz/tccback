namespace TccBack.Domain.Entities
{
	public class Notification : Entity
	{
		public Notification()
		{
			
		}
		
		public Notification(string title, string message)
		{
			IsReaded = false;
			Title = title;
			Message = message;
		}

		public bool IsReaded { get; set; }
		public string Title { get; set; }
		public string Message { get; set; }

		public void Read()
		{
			IsReaded = true;
		}
	}
}