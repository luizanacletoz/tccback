using System.Globalization;
using AutoMapper;
using TccBack.Domain.Commands.AccountCommands.Output;
using TccBack.Domain.Commands.BudgetCommands.Output;
using TccBack.Domain.Commands.BudgetResponseCommands.Output;
using TccBack.Domain.Commands.CompanyCommands.Output;
using TccBack.Domain.Commands.NotificationCommands.Output;
using TccBack.Domain.Commands.ResponseItemCommands.Output;
using TccBack.Domain.Entities;
using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.AutoMapper
{
	public class EntityToCommandMappingProfile : Profile
	{
		public EntityToCommandMappingProfile()
		{
			var culture = CultureInfo.CreateSpecificCulture("pt-BR");
			
			//Budget
			CreateMap<Budget, CreateBudgetCommandResult>()
				.ForMember(dest => dest.Items,
					opt => opt.MapFrom(src => src.Items));

			CreateMap<Budget, GetBudgetByIdToClientCommandResult>()
				.ForMember(dest => dest.CreatedAt,
					opt => opt.MapFrom(src => src.CreatedAt.ToString("g", culture)));

			CreateMap<Budget, GetBudgetByIdToCompanyCommandResult>()
				.ForMember(dest => dest.Client,
				opt => opt.MapFrom(src => src.Client.Name))
				.ForMember(dest => dest.Responses,
				opt => opt.MapFrom(src => src.Responses));

			//BudgetResponse
			CreateMap<BudgetResponse, CreateBudgetResponseCommandResult>()
				.ForMember(dest => dest.Value,
					opt => opt.MapFrom(src => src.Value.ToString("C", culture)));
			
			CreateMap<BudgetResponse, BudgetResponseCommandResult>()
				.ForMember(dest => dest.Value,
					opt => opt.MapFrom(src => src.Value.ToString("C", culture)));
			
			//BudgetResponseItem
			CreateMap<ResponseItem, BudgetResponseItemCommandResult>()
				.ForMember(dest => dest.TotalValue,
					opt => opt.MapFrom(src => src.TotalValue.ToString("C", culture)))
				.ForMember(dest => dest.UnityValue,
					opt => opt.MapFrom(src => src.UnityValue.ToString("C", culture)));
			
			
			//Client
			CreateMap<Client, GetCurrentClientCommandResult>();
			
			//Company
			CreateMap<Company, GetCurrentCompanyCommandResult>()
				.ForMember(dest => dest.Credits,
				opt => opt.MapFrom(src => src.Credits.ToString("C", culture)));
			CreateMap<Company, CompanyCommandResult>();
			
			//ClientNotification
			CreateMap<ClientNotification, ClientNotificationCommandResult>()
				.ForMember(dest => dest.CreatedAt,
					opt => opt.MapFrom(src => src.CreatedAt.ToString("d")));
			
			//CompanyNotification
			CreateMap<CompanyNotification, CompanyNotificationCommandResult>()
				.ForMember(dest => dest.CreatedAt,
					opt => opt.MapFrom(src => src.CreatedAt.ToString("d")));
		}
	}
}