using AutoMapper;

namespace TccBack.Domain.AutoMapper
{
	public class AutoMapperConfig
	{
		public static MapperConfiguration RegisterMappings()
		{
			return new MapperConfiguration(cfg =>
			{
				cfg.AddProfile(new CommandToEntityMappingProfile());
				cfg.AddProfile(new EntityToCommandMappingProfile());
			});
		}
	}
}