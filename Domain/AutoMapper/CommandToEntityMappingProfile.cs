using System;
using System.Runtime.CompilerServices;
using AutoMapper;
using TccBack.Domain.Commands.AccountCommands.Input;
using TccBack.Domain.Commands.BudgetCommands.Input;
using TccBack.Domain.Entities;

namespace TccBack.Domain.AutoMapper
{
	public class CommandToEntityMappingProfile : Profile
	{
		public CommandToEntityMappingProfile()
		{
			//ApplicationUser - Client
			CreateMap<RegisterClientCommand, Client>()
				.ForMember(dest => dest.UserName,
					opt => opt.MapFrom(src => src.Email));
			
			//ApplicationUser - Company
			CreateMap<RegisterCompanyCommand, Company>()
				.ForMember(dest => dest.UserName,
					opt => opt.MapFrom(src => src.Email));
			
			//Budget
			CreateMap<CreateBudgetCommand, Budget>();

		}
	}
}