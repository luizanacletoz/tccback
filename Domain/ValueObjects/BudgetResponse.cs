using System;
using System.Collections.Generic;
using Swashbuckle.AspNetCore.Swagger;
using TccBack.Domain.DenormalizedReferences.Classes;
using TccBack.Domain.Entities;

namespace TccBack.Domain.ValueObjects
{
	public class BudgetResponse
	{
		public BudgetResponse()
		{
			
		}
		
		public BudgetResponse(Company company, decimal value)
		{
			Company = company;
			Value = value;
			ResponseItems = new List<ResponseItem>();
			CreatedAt = DateTime.Now;
		}
		
		public BudgetResponse(Company company, decimal value, IList<ResponseItem> responseItems)
		{
			Company = company;
			ResponseItems = responseItems;
			Value = value;
			CreatedAt = DateTime.Now;
		}
		
		public DenormalizedCompany<Company> Company { get; set; }
		public IList<ResponseItem> ResponseItems { get; set; }
		public decimal Value { get; set; }
		public DateTime CreatedAt { get; set; }

		public void AddReponseItem(ResponseItem responseItem)
		{
			if (ResponseItems == null)
				ResponseItems = new List<ResponseItem>();
			
			ResponseItems.Add(responseItem);
		}
		
		public void AddReponseItem(IList<ResponseItem> responseItems)
		{
			if (ResponseItems == null)
				ResponseItems = new List<ResponseItem>();

			foreach (var responseItem in responseItems)
			{
				ResponseItems.Add(responseItem);
			}
		}	

	
	}
}