namespace TccBack.Domain.ValueObjects
{
	public class ResponseItem : Item
	{
		public decimal TotalValue { get; set; }
		public decimal UnityValue { get; set; }
		public bool HaveItem { get; set; }

		public ResponseItem(
			int amount, 
			string name, 
			decimal unityValue
			) : base(amount, name)
		{
			HaveItem = amount >= 1;
			UnityValue = unityValue;
			TotalValue = amount * unityValue;
			
		}
		
	}
}