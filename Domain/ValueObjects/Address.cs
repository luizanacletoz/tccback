namespace TccBack.Domain.ValueObjects
{
	public class Address
	{
		public string Cep { get; set; }
		public string Uf { get; set; }
		public string City { get; set; }
		public string Neighborhood { get; set; }
		public string Street { get; set; }
	}
}