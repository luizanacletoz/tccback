namespace TccBack.Domain.ValueObjects
{
	public class Item
	{
		public Item(int amount, string name)
		{
			Amount = amount;
			Name = name;
		}

		public int Amount { get; set; }
		public string Name { get; set; }
		public string Type { get; set; }
	}
}