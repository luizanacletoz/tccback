using FluentValidation;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using Raven.Identity;
using TccBack.Domain.Commands.AccountCommands.Input;

namespace TccBack.Domain.Validators.Account
{
	public class RegisterCompanyValidator: AbstractValidator<RegisterCompanyCommand>
	{

		private readonly IAsyncDocumentSession _dbSession;
		
		public RegisterCompanyValidator(IAsyncDocumentSession dbSession)
		{
			_dbSession = dbSession;

			RuleFor(x => x.Email)
				.EmailAddress().WithMessage("Deve ser um e-mail válido")
				.NotEmpty().WithMessage("Não pode ser vazio")
				.Must(ExistEmail).WithMessage("Já está cadastrado na plataforma");

			RuleFor(x => x.Password)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.Name)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.Cep)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.Uf)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.City)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.Neighborhood)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.Street)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.Lat)
				.NotNull()
				.WithMessage("Deve ser informado")
				.Must(x => x != 0).WithMessage("Deve ser informado");

			RuleFor(x => x.Long)
				.NotNull()
				.WithMessage("Deve ser informado")
				.Must(x => x != 0).WithMessage("Deve ser informado");
		}

		private bool ExistEmail(string email)
		{
			return !_dbSession.Query<IdentityUserByUserName>()
				.AnyAsync(x => x.UserName.Equals(email)).Result;
		}
		
	}
}