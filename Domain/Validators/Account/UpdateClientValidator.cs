using FluentValidation;
using TccBack.Domain.Commands.AccountCommands.Input;

namespace TccBack.Domain.Validators.Account
{
	public class UpdateClientValidator : AbstractValidator<UpdateClientCommand>
	{
		public UpdateClientValidator()
		{
			RuleFor(x => x.Name)
				.NotEmpty()
				.WithMessage("O nome deve ser informado");
		}
	}
}