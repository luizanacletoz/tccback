using FluentValidation;
using TccBack.Domain.Commands.AccountCommands.Input;

namespace TccBack.Domain.Validators.Account
{
	public class UpdateCompanyValidator : AbstractValidator<UpdateCompanyCommand>
	{
		public UpdateCompanyValidator()
		{
			RuleFor(x => x.Name)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.Cep)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.Uf)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.City)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.Neighborhood)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.Street)
				.NotEmpty().WithMessage("Deve ser informado");
			
		}
	}
}