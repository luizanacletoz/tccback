using FluentValidation;
using TccBack.Domain.Commands.AccountCommands.Input;

namespace TccBack.Domain.Validators.Account
{
    public class LoginValidator : AbstractValidator<LoginCommand>
    {
        public LoginValidator()
        {
            RuleFor(x => x.Email)
                .EmailAddress().WithMessage("Deve ser um e-mail válido")
                .NotEmpty().WithMessage("Não pode ser vazio");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Deve ser informado");
        }
    }
}