using FluentValidation;
using TccBack.Domain.Commands.AccountCommands.Input;

namespace TccBack.Domain.Validators.Account
{
	public class UpdateUserPasswordValidator : AbstractValidator<UpdateUserPasswordCommand>
	{
		public UpdateUserPasswordValidator()
		{
			RuleFor(x => x.CurrentPassword)
				.NotEmpty().WithMessage("Deve ser informado");
			
			RuleFor(x => x.Password)
				.NotEmpty().WithMessage("Deve ser informado");

			RuleFor(x => x.SamePassword)
				.NotEmpty().WithMessage("Deve ser informado")
				.Equal(x => x.Password)
				.WithMessage("Os dois campos devem ser iguais");
		}
	}
}