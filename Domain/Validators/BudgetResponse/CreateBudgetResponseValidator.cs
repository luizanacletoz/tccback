using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using FluentValidation;
using TccBack.Domain.Commands.BudgetResponseCommands.Input;
using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.Validators.BudgetResponse
{
	public class CreateBudgetResponseValidator : AbstractValidator<CreateBudgetResponseCommand>
	{
		public CreateBudgetResponseValidator()
		{
			/*RuleFor(x => x.Value)
				.GreaterThanOrEqualTo(5)
				.WithMessage("O valor deve ser maior do que R$5,00");*/
			
			RuleFor(x => x.ResponseItems)
				.Must(ValidateProperties)
				.WithMessage("Você deve preencher todos os itens do orçamento");
		}
		
		private bool ValidateProperties(IList<ResponseItem> responseItems)
		{
			if (responseItems == null || responseItems.Count().Equals(0))
				return false;
			
			foreach (var responseItem in responseItems)
			{
				if (responseItem.HaveItem){
					if (
						responseItem.Name.Length.Equals(0) ||
						responseItem.Amount == 0 ||
						responseItem.UnityValue < 0.1m)
					{
						return false;
					}
				}
			}

			return true;
			
		}
	}
}