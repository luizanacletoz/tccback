using FluentValidation;
using TccBack.Domain.Commands.CreditCommands.Input;

namespace TccBack.Domain.Validators.Credit
{
	public class AddCreditValidator : AbstractValidator<AddCreditCommand>
	{
		public AddCreditValidator()
		{
			RuleFor(x => x.Credits)
				.GreaterThan(10m)
				.WithMessage("O valor minimo é de R$ 10,00");

			RuleFor(x => x.Installments)
				.GreaterThan(0)
				.WithMessage("Deve ser a vista ou até 12 vezes")
				.LessThanOrEqualTo(12)
				.WithMessage("Deve ser a vista ou até 12 vezes");

			RuleFor(x => x.CreditCard.Brand)
				.Configure(x => x.PropertyName = "Brand")
				.NotEmpty()
				.WithMessage("A bandeira deve ser informada");

			RuleFor(x => x.CreditCard.Holder)
				.Configure(x => x.PropertyName = "Holder")
				.NotEmpty()
				.WithMessage("O nome do dono do cartão deve ser informado");

			RuleFor(x => x.CreditCard.CardNumber)
				.Configure(x => x.PropertyName = "CardNumber")
				.NotEmpty()
				.WithMessage("Deve ser um número de cartão de crédito válido");
//				.CreditCard()
//				.WithMessage("Deve ser um cartão de crédito válido");

			RuleFor(x => x.CreditCard.ExpirationDate)
				.Configure(x => x.PropertyName = "ExpirationDate")
				.NotEmpty()
				.WithMessage("A data de expiração deve ser informada")
				.Length(7)
				.WithMessage("A data de expiração deve estar no formato válido");

			RuleFor(x => x.CreditCard.SecurityCode)
				.Configure(x => x.PropertyName = "SecurityCode")
				.NotEmpty()
				.WithMessage("O código de segurança deve ser informado")
				.Length(3)
				.WithMessage("O código de segurança deve conter 3 digitos");
		}
	}
}