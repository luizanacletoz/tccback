namespace TccBack.Domain.Constants
{
	public static class Roles
	{
		public const string Client = "client";
		public const string Administrator = "administrator";
		public const string Company = "company";
	}
}