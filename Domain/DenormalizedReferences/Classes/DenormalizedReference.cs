using System;
using TccBack.Domain.DenormalizedReferences.Interfaces;

namespace TccBack.Domain.DenormalizedReferences.Classes
{
	public class DenormalizedReference<T> where T : IDenormalizedReference
	{
		public string Id { get; set; }
		public string Name { get; set; }
		
		public static implicit operator DenormalizedReference<T>(T doc)
		{
			return new DenormalizedReference<T>
			{
				Id = doc.Id,
				Name = doc.Name,
			};
		}

		public bool Equals(T obj)
		{
			return obj.Id.Equals(Id);
		}
	}
}