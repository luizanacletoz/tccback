using TccBack.Domain.DenormalizedReferences.Interfaces;

namespace TccBack.Domain.DenormalizedReferences.Classes
{
	public class DenormalizedCompany<T> where T : IDenormalizedCompany
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string ImageUrl { get; set; }

		public static implicit operator DenormalizedCompany<T>(T doc)
		{
			return new DenormalizedCompany<T>
			{
				Id = doc.Id,
				Name = doc.Name,
				ImageUrl = doc.ImageUrl
			};
		}
	}
}