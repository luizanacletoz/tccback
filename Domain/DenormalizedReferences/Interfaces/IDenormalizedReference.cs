using System;

namespace TccBack.Domain.DenormalizedReferences.Interfaces
{
	public interface IDenormalizedReference
	{
		string Id { get; set; }
		string Name { get; set; }
	}
}