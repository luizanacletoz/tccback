using System;

namespace TccBack.Domain.DenormalizedReferences.Interfaces
{
	public interface IDenormalizedCompany
	{
		string Id { get; set; }
		string Name { get; set; }
		string ImageUrl { get; set; }
	}
}