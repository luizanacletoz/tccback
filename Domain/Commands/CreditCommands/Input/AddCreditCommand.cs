using TccBack.Services.Payment;
using TccBack.Services.Payment.PaymentEntities;

namespace TccBack.Domain.Commands.CreditCommands.Input
{
	public class AddCreditCommand 
	{
		public decimal Credits { get; set; }
		public int Installments { get; set; }
		public CreditCard CreditCard { get; set; }
	}
}