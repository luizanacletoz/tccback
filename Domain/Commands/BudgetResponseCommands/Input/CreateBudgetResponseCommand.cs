using System.Collections.Generic;
using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.Commands.BudgetResponseCommands.Input
{
	public class CreateBudgetResponseCommand
	{
		public string BudgetId { get; set; }
		public IList<ResponseItem> ResponseItems { get; set; }
		public decimal Value { get; set; }
	}
}