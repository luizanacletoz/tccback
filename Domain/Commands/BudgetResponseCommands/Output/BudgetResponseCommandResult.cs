using System.Collections.Generic;
using TccBack.Domain.Commands.ResponseItemCommands.Output;
using TccBack.Domain.DenormalizedReferences.Classes;
using TccBack.Domain.Entities;
using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.Commands.BudgetResponseCommands.Output
{
	public class BudgetResponseCommandResult
	{
		public DenormalizedCompany<Company> Company { get; set; }
		public IList<BudgetResponseItemCommandResult> ResponseItems { get; set; }
		public string Value { get; set; }
	}
}