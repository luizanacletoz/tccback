using System.Collections.Generic;
using TccBack.Domain.DenormalizedReferences.Classes;
using TccBack.Domain.Entities;
using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.Commands.BudgetResponseCommands.Output
{
	public class CreateBudgetResponseCommandResult
	{
		public DenormalizedCompany<Company> Company { get; set; }
		public IList<ResponseItem> ResponseItems { get; set; }
		public string Value { get; set; }
	}
}