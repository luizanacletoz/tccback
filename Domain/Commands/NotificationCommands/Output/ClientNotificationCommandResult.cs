namespace TccBack.Domain.Commands.NotificationCommands.Output
{
	public class ClientNotificationCommandResult
	{
		public string Id { get; set; }
		public string CreatedAt { get; set; }
		public bool IsReaded { get; set; }
		public string Title { get; set; }
		public string Message { get; set; }
		public string ClientId { get; set; }
		public string BudgetId { get; set; }
	}
}