using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.Commands.AccountCommands.Output
{
	public class GetCurrentCompanyCommandResult
	{
		public string Id { get; set; }
		public string Email { get; set; }
		public string Name { get; set; }
		public string Cnpj { get; set; }
		public string ImageUrl { get; set; }
		public string PhoneNumber { get; set; }
		public string Credits { get; set; }
		public Address Address { get; set; }
		public double Lat { get; set; }
		public double Long { get; set; }
		
		public string Role => "company";
	}
}