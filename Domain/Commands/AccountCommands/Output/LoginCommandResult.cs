using System;

namespace TccBack.Domain.Commands.AccountCommands.Output
{
    public class LoginCommandResult
    {
        
        public LoginCommandResult(string type, string accessToken, DateTime expirationDate)
        {
            Type = type;
            AccessToken = accessToken;
            ExpirationDate = expirationDate;
        }
		
        public string Type { get; set; }
        public string AccessToken { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}