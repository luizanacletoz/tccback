namespace TccBack.Domain.Commands.AccountCommands.Output
{
	public class MessageLoginCommandResult
	{
		public string Message { get; set; }
	}
}