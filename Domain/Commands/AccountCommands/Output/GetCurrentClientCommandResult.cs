namespace TccBack.Domain.Commands.AccountCommands.Output
{
	public class GetCurrentClientCommandResult
	{
		public string Id { get; set; }
		public string Email { get; set; }
		public string Name { get; set; }
		public string Role => "client";
	}
}