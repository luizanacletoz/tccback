namespace TccBack.Domain.Commands.AccountCommands.Output
{
	public class GetAllUserCommandResult
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
	}
}