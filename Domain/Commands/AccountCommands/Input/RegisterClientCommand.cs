
namespace TccBack.Domain.Commands.AccountCommands.Input
{
	public class RegisterClientCommand 
	{
		public string Name { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public string Cpf { get; set; }

	}
}