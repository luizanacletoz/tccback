namespace TccBack.Domain.Commands.AccountCommands.Input
{
    public class LoginCommand
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}