namespace TccBack.Domain.Commands.AccountCommands.Input
{
	public class UpdateCompanyCommand
	{
		public string Name { get; set; }
		
		public string Cep { get; set; }
		public string Uf { get; set; }
		public string City { get; set; }
		public string Neighborhood { get; set; }
		public string Street { get; set; }
		public string PhoneNumber { get; set; }
	}
}