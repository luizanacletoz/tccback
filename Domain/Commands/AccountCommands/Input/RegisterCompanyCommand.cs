namespace TccBack.Domain.Commands.AccountCommands.Input
{
	public class RegisterCompanyCommand
	{
		public string Name { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public string Cnpj { get; set; }
		
		public double? Lat { get; set; }
		public double? Long { get; set; }
		
		
		public string Cep { get; set; }
		public string Uf { get; set; }
		public string City { get; set; }
		public string Neighborhood { get; set; }
		public string Street { get; set; }
	}
}