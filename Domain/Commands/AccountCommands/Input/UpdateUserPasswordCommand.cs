namespace TccBack.Domain.Commands.AccountCommands.Input
{
	public class UpdateUserPasswordCommand
	{
		public string CurrentPassword { get; set; }
		public string Password { get; set; }
		public string SamePassword { get; set; }
	}
}