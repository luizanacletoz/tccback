namespace TccBack.Domain.Commands.AccountCommands.Input
{
	public class UpdateClientCommand
	{
		public string Name { get; set; }
	}
}