using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.Commands.CompanyCommands.Output
{
	public class CompanyCommandResult
	{
		public string Id { get; set; }
		public string Email { get; set; }
		public string Name { get; set; }
		public string Cnpj { get; set; }
		public string ImageUrl { get; set; }
		public string PhoneNumber { get; set; }
		public Address Address { get; set; }
		public double Lat { get; set; }
		public double Long { get; set; }
	}
}