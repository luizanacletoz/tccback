using System.Collections.Generic;
using TccBack.Domain.DenormalizedReferences.Classes;
using TccBack.Domain.Entities;
using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.Commands.BudgetCommands.Output
{
	public class CreateBudgetCommandResult
	{
		public string Id { get; set; }
		public bool IsOpen { get; set; }
		public Company SelectedCompany { get; set; }
		public DenormalizedReference<Client> Client { get; set; }
		public IList<Item> Items { get; set; }
		public IList<DenormalizedCompany<Company>> Companies { get; set; }
	}
}