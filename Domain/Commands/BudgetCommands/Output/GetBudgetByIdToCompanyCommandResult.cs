using System.Collections.Generic;
using TccBack.Domain.Commands.BudgetResponseCommands.Output;
using TccBack.Domain.Entities;
using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.Commands.BudgetCommands.Output
{
	public class GetBudgetByIdToCompanyCommandResult
	{
		public string Id { get; set; }
		public string Client { get; set; }
		
		public bool IsOpen { get; set; }
		public Company SelectedCompany { get; set; }
		public IList<BudgetResponseCommandResult> Responses { get; set; }
		
		public IList<Item> Items { get; set;  }
	}
}