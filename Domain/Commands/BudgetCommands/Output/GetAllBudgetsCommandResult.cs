using System.Collections.Generic;
using TccBack.Domain.DenormalizedReferences.Classes;
using TccBack.Domain.Entities;
using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.Commands.BudgetCommands.Output
{
	public class GetAllBudgetsCommandResult
	{
		public string Id { get; set; }
		public DenormalizedReference<Client> Client { get; set; }
		public bool IsOpen { get; set; }
		public Company SelectedCompany { get; set; }
		public string CreatedAt { get; set; }
		public string BestProposal { get; set; }
	}
}