using System.Collections.Generic;
using TccBack.Domain.ValueObjects;

namespace TccBack.Domain.Commands.BudgetCommands.Input
{
	public class CreateBudgetCommand
	{
		public int Km { get; set; }
		public double Lat { get; set; }
		public double Long { get; set; }
		public IList<Item> Items { get; set; }
	}
}