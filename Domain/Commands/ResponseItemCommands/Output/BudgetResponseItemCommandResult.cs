namespace TccBack.Domain.Commands.ResponseItemCommands.Output
{
	public class BudgetResponseItemCommandResult
	{
		public int Amount { get; set; }
		public string Name { get; set; }
		public string TotalValue { get; set; }
		public string UnityValue { get; set; }
		public bool HaveItem { get; set; }
	}
}