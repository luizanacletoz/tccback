namespace TccBack.Domain.Commands.ResponseItemCommands.Output
{
	public class CreateBudgetResponseResponseItemCommandResult
	{
		public int Amount { get; set; }
		public string Name { get; set; }
		public decimal TotalValue { get; set; }
		public decimal UnityValue { get; set; }
		public bool HaveItem { get; set; }

	}
}