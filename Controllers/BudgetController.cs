using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using Raven.Client.Documents;
using Raven.Client.Documents.Linq;
using Raven.Client.Documents.Session;
using TccBack.Domain.Commands.BudgetCommands.Input;
using TccBack.Domain.Commands.BudgetCommands.Output;
using TccBack.Domain.Entities;
using TccBack.Services;

namespace TccBack.Controllers
{
	public class BudgetController : ApplicationController
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly MessageService _messageService;
		private readonly ILogger<BudgetController> _logger;
		
		public BudgetController(IAsyncDocumentSession dbSession, IMapper mapper, UserManager<ApplicationUser> userManager, MessageService messageService, ILogger<BudgetController> logger) : base(dbSession, mapper)
		{
			_userManager = userManager;
			_messageService = messageService;
			_logger = logger;
		}
		
		// POST: api/Budget
		[HttpPost]
		[Authorize(Roles = "client")]
		[ProducesResponseType(200, Type = typeof(CreateBudgetCommandResult))]
		[ProducesResponseType(400)]
		[Route("api/budgets")]
		public async Task<IActionResult> Post(CreateBudgetCommand command)
		{
			var user = await GetCurrentUser();
			
			var budget = Mapper.Map<CreateBudgetCommand, Budget>(command);

			budget.Client = user as Client;
			
			await DbSession.StoreAsync(budget);

			var companies = await DbSession.Query<Company>()
				.Spatial(
					factory => factory.Point(x => x.Lat, x => x.Long),
					criteria => criteria.WithinRadius(command.Km, command.Lat, command.Long))
				.ToListAsync();

			if (companies.Count > 0)
				budget.AddCompanies(companies);
			else
				AddMessage("Companies", "Não há empresas na área informada");

			var stringItems = "";
			foreach (var budgetItem in budget.Items)
			{
				stringItems += $"{budgetItem.Amount} {budgetItem.Name}<br>";
			}
			
			//Enviar email para as companias
			foreach (var company in companies)
			{
				if (company.IsPreRegister)
				{
					var content = $"Olá, {company.Name} <br>" +
					              $"Temos um novo orçamento para você :D<br>" +
					              $"{stringItems} <br>" +
					              $"<a href=\"#\">Clique aqui</a> para responder o orçamento e ganhar novos clientes!";
				
					await _messageService.SendEmailAsync(company.Email, "Novo orçamento - TccBack", content);
				}
				else
				{
					var content = $"Olá, {company.Name} <br>" +
					              $"Temos um novo orçamento para você :D<br>" +
					              $"{stringItems} <br>";
				
					await _messageService.SendEmailAsync(company.Email, "Novo orçamento - TccBack", content);
					
					var notification = new CompanyNotification($"Novo orçamento de {budget.Client.Name}", 
						$"Você recebeu um novo orçamento com {budget.Items.Count()} tipos de produtos, clique na notificação para fazer uma proposta!",
						company.Id,
						budget.Id);

					await DbSession.StoreAsync(notification);
				}
				
			}
			
			if (!ModelState.IsValid)
				return BadRequest(ModelState);
			
			return Ok(Mapper.Map<Budget, CreateBudgetCommandResult>(budget));
		}

		[HttpPut]
		[Authorize(Roles = "client, administrator")]
		[ProducesResponseType(204)]
		[ProducesResponseType(404)]
		[Route("api/budgets/{id}")]
		public async Task<IActionResult> Toggle(string id)
		{

			var user = await GetCurrentUser();
			var budget = await DbSession
				.Query<Budget>()
				.SingleOrDefaultAsync(x => x.Id.Equals(id));

			if (budget == null) return NotFound();

			if (!user.Id.Equals(budget.Client.Id))
				if (!user.IsAdmin())
					return Unauthorized();
				
			budget.IsOpen = !budget.IsOpen;
			budget.UpdatedAt = DateTime.Now;
				
			return Ok();
		}

		[HttpGet]
		[Authorize(Roles = "company, administrator")]
		[ProducesResponseType(200)]
		[ProducesResponseType(404)]
		[Route("api/companies/{companyId}/budgets")]
		public async Task<IActionResult> GetCompanyBudgets(string companyId, [FromQuery] GetAllBudgetsByCompanyCommand command)
		{
			var user = await GetCurrentUser();
			if (!user.Id.Equals(companyId))
				if (!user.IsAdmin())
					return Unauthorized();


			var budgetsQuery = DbSession
				.Query<Budget>()
				.Where(x => x.Companies.Contains(user as Company))
				.Where(x => x.IsOpen.Equals(true));
				
			if (command.Answered){
				budgetsQuery = budgetsQuery.Where(x => x.Responses.Any(y => y.Company.Id.Equals(companyId)));
			}else{
				budgetsQuery = budgetsQuery.Where(x => x.Responses.Any(y => !y.Company.Id.Equals(companyId)));
			}

			var budgets = await budgetsQuery.Select(x => new GetAllBudgetsCommandResult
				{
					Id = x.Id,
					Client = x.Client,
					IsOpen = x.IsOpen,
					SelectedCompany = x.SelectedCompany,
					BestProposal = x.BestProposal.ToString("C", CultureInfo.CreateSpecificCulture("pt-BR")),
					CreatedAt = x.CreatedAt.ToString("g")
				})
				.OrderByDescending(x => x.CreatedAt)
				.ToListAsync();
			
			return Ok(budgets);
		}

		[HttpGet]
		[Authorize(Roles = "company, administrator")]
		[ProducesResponseType(200)]
		[ProducesResponseType(404)]
		[Route("api/companies/{companyId}/budgets/{budgetId}")]
		public async Task<IActionResult> GetCompanyBudgetsById(string companyId, string budgetId)
		{
			var user = await GetCurrentUser();
			if (!user.Id.Equals(companyId))
				if (!user.IsAdmin())
					return Unauthorized();

			var budget = await DbSession
				.Query<Budget>()
				.Where(x => x.Companies.Contains(user as Company))
				.SingleOrDefaultAsync(x => x.Id.Equals(budgetId));

			if(budget.Responses != null){
				budget.Responses = budget.Responses.Where(x => x.Company.Id.Equals(companyId)).ToList();
			}

			return Ok(Mapper.Map<Budget, GetBudgetByIdToCompanyCommandResult>(budget));
		}
		
		[HttpGet]
		[Authorize(Roles = "client")]
		[ProducesResponseType(200)]
		[ProducesResponseType(204)]
		[Route("api/budgets")]
		public async Task<IActionResult> Get(bool isOpen = true)
		{
			
			var user = await GetCurrentUser();
			var query = DbSession
				.Query<Budget>()
				.Where(x => x.Client.Id.Equals(user.Id))
				.Where(x => x.IsOpen.Equals(isOpen));
			
			var budgets = await query
				.Select(x => new GetAllBudgetsCommandResult
				{
					Id = x.Id,
					Client = x.Client,
					IsOpen = x.IsOpen,
					SelectedCompany = x.SelectedCompany,
					CreatedAt = x.CreatedAt.ToString("g"),
					BestProposal = x.BestProposal.ToString("C", CultureInfo.CreateSpecificCulture("pt-BR"))
				})
				.OrderByDescending(x => x.CreatedAt)
				.ToListAsync();
			
			return Ok(budgets);
		}
		
		[HttpGet]
		[Authorize(Roles = "client, administrator")]
		[ProducesResponseType(typeof(GetBudgetByIdToClientCommandResult), 200)]
		[ProducesResponseType(404)]
		[Route("api/budgets/{id}")]
		public async Task<IActionResult> GetById(string id)
		{
			var user = await GetCurrentUser();
			var budget = await DbSession
				.LoadAsync<Budget>(id);

			if (!budget.Client.Id.Equals(user.Id))
				if(!user.IsAdmin())
					return NotFound();

			if (budget.Responses != null && budget.Responses.Count > 0)
				budget.Responses = budget.Responses.OrderBy(x => x.Value).ToList();
			
			return Ok(Mapper.Map<Budget, GetBudgetByIdToClientCommandResult>(budget));
		}
	}
}