using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Raven.Client.Documents.Session;
using TccBack.Domain.Commands.BudgetResponseCommands.Input;
using TccBack.Domain.Commands.BudgetResponseCommands.Output;
using TccBack.Domain.Entities;
using TccBack.Domain.ValueObjects;

namespace TccBack.Controllers
{
	public class BudgetResponseController : ApplicationController
	{
		
		private ILogger<BudgetResponseController> _logger;

		public BudgetResponseController(IAsyncDocumentSession dbSession, IMapper mapper, ILogger<BudgetResponseController> logger) : base(dbSession, mapper)
		{
			_logger = logger;
		}

		[HttpPost]
		[Authorize(Roles = "company")]
		[ProducesResponseType(typeof(CreateBudgetResponseCommandResult), 200)]
		[ProducesResponseType(400)]
		[ProducesResponseType(404)]
		[Route("api/budgetResponses")]
		public async Task<IActionResult> CreateResponse(CreateBudgetResponseCommand command)
		{
			var responseValue = 5m;

			var company = await GetCurrentCompany();
			var budget = await DbSession.LoadAsync<Budget>(command.BudgetId);
			if (!budget.CompanyInBudget(company))
				return Unauthorized();

			if(company.Credits < responseValue)
				AddMessage("Credits", "Você não tem créditos suficientes");

			if (!budget.IsOpen)
				AddMessage("Budget", "O orçamento já está fechado");

			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			if(budget.Responses != null){
				foreach (var budgetResponse in budget.Responses)
				{
					if (budgetResponse.Value > command.Value)
					{
						budget.BestProposal = command.Value;
					}
				}
			}else{
				budget.BestProposal = command.Value;
			}
						
			var createdResponse = budget.CreateResponse(company, command.Value ,command.ResponseItems);

			var clientNotification = new ClientNotification(
				$"Resposta de orçamento",
				$"A loja {company.Name} respondeu seu orçamento com valor total de {command.Value:C}",
				budget.Client.Id,
				budget.Id);

			await DbSession.StoreAsync(clientNotification);
			
			company.RemoveCredits(responseValue);

			return Ok(Mapper.Map<BudgetResponse, CreateBudgetResponseCommandResult>(createdResponse));
		}
	}
}