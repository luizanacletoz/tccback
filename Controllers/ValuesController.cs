using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Raven.Client.Documents.Session;
using TccBack.Domain.Commands.AccountCommands.Input;
using TccBack.Domain.Commands.AccountCommands.Output;
using TccBack.Domain.Constants;
using TccBack.Domain.Entities;

namespace TccBack.Controllers
{
	public class ValuesController : ApplicationController
	{
		public ValuesController(IAsyncDocumentSession dbSession, IMapper mapper) : base(dbSession, mapper)
		{
		}
		
		// POST: api/Account/Company
		[HttpGet]
		[AllowAnonymous]	
		[Route("api/values")]
		public IActionResult Get()
		{
			return Ok(new { Version = "1.0" });
		}
	}
}