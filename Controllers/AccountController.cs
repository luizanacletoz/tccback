using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using Sparrow.Json;
using TccBack.Domain.Commands.AccountCommands.Input;
using TccBack.Domain.Commands.AccountCommands.Output;
using TccBack.Domain.Constants;
using TccBack.Domain.Entities;
using TccBack.Domain.ValueObjects;
using TccBack.Services;

namespace TccBack.Controllers
{
	[Authorize]
	public class AccountController : ApplicationController
	{

		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;
		private readonly JwtService _jwtService;
		private readonly MessageService _emailSender;
		private readonly IConfiguration _configuration;
		private readonly ILogger<AccountController> _logger;
		
		public AccountController(IAsyncDocumentSession dbSession, 
			IMapper mapper,UserManager<ApplicationUser> userManager, 
			SignInManager<ApplicationUser> signInManager, 
			JwtService jwtService, MessageService emailSender, 
			IConfiguration configuration, ILogger<AccountController> logger) : base(dbSession, mapper)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_jwtService = jwtService;
			_emailSender = emailSender;
			_configuration = configuration;
			_logger = logger;
		}
		
		
		// POST: api/Account/auth
		[HttpPost]
		[AllowAnonymous]
		[ProducesResponseType(200, Type = typeof(LoginCommandResult))]
		[ProducesResponseType(400, Type = typeof(MessageLoginCommandResult))]
		[Route("api/accounts/auth")]
		public async Task<IActionResult> Post(LoginCommand command)
		{
			var user = await _userManager.FindByNameAsync(command.Email);
			if (user != null)
			{
				var result = await _signInManager.CheckPasswordSignInAsync(user, command.Password, false);
				if (result.Succeeded)
				{
					var claims = new[]
					{
						new Claim(JwtRegisteredClaimNames.Sid, user.Id.ToString()),
						new Claim(JwtRegisteredClaimNames.Email, user.Email),
						new Claim(ClaimTypes.Role, string.Join(",", user.Roles)), 
					};

					if (await _userManager.IsInRoleAsync(user, Roles.Company) &&
					    !await _userManager.IsEmailConfirmedAsync(user))
						return BadRequest(new MessageLoginCommandResult { Message = "A conta de usuário ainda não foi ativada!"});
				
					return Ok(new LoginCommandResult("Bearer", _jwtService.GenerateToken(claims), DateTime.Now.AddDays(14)));
				}
			}
			
			return BadRequest(new MessageLoginCommandResult{ Message = "Usuário ou senha incorretos"});
		}
		
		[HttpGet]
		[AllowAnonymous]
		[ProducesResponseType(200)]
		[ProducesResponseType(404)]
		[Route("api/accounts/info")]
		public async Task<IActionResult> Get()
		{
			var client = await GetCurrentClient();
			if (client != null)
				return Ok(Mapper.Map<Client, GetCurrentClientCommandResult>(client));
			
			var company = await GetCurrentCompany();
			if(company != null)
				return Ok(Mapper.Map<Company, GetCurrentCompanyCommandResult>(company));

			return NotFound();
		}
		
		[HttpPost]
		[AllowAnonymous]
		[ProducesResponseType(200, Type = typeof(LoginCommandResult))]
		[ProducesResponseType(400)]
		[Route("api/accounts/clients")]
		public async Task<IActionResult> Post(RegisterClientCommand command)
		{
			var user = Mapper.Map<RegisterClientCommand, Client>(command);

			var createdResult = await _userManager.CreateAsync(user, command.Password);

			await _userManager.AddToRoleAsync(user, Roles.Client);
			
			if (!createdResult.Succeeded)
			{
				return BadRequest(createdResult.Errors);
			}
			
			var result = await _signInManager.CheckPasswordSignInAsync(user, command.Password, false);
			if (result.Succeeded)
			{
				var claims = new[]
				{
					new Claim(JwtRegisteredClaimNames.Sid, user.Id.ToString()),
					new Claim(JwtRegisteredClaimNames.Email, user.Email),
					new Claim(ClaimTypes.Role, Roles.Client), 
				};
				
				return Ok(new LoginCommandResult("Bearer", _jwtService.GenerateToken(claims), DateTime.Now.AddDays(14)));
			}
			
			DbSession.Delete(user.Id);
			return BadRequest(user);
		}
		
		[HttpPut]
		[Authorize(Roles = Roles.Client)]
		[ProducesResponseType(200)]
		[ProducesResponseType(400)]
		[Route("api/accounts/clients")]
		public async Task<IActionResult> Put(UpdateClientCommand command)
		{
			var user = await GetCurrentClient();
			user.Name = command.Name;

			return Ok();
		}
		
		[HttpPost]
		[AllowAnonymous]
		[ProducesResponseType(200, Type = typeof(MessageLoginCommandResult))]
		[ProducesResponseType(400)]
		[Route("api/accounts/companies")]
		public async Task<IActionResult> Post(RegisterCompanyCommand command)
		{
			var user = Mapper.Map<RegisterCompanyCommand, Company>(command);
			user.IsPreRegister = false;
			user.Address = new Address
			{
				Cep = command.Cep,
				City = command.City,
				Neighborhood = command.Neighborhood,
				Street = command.Street,
				Uf = command.Uf
			};
			
			var createdResult = await _userManager.CreateAsync(user, command.Password);

			await _userManager.AddToRoleAsync(user, Roles.Company);
			
			if (!createdResult.Succeeded)
			{
				return BadRequest(createdResult.Errors);
			}

			var code = (await _userManager.GenerateEmailConfirmationTokenAsync(user)).Replace("/", "-").Replace("+", "_");
			await _emailSender.SendActivationEmailAsync(user, $"Ativação de conta - OrçaFácil", "", $"{_configuration["ApiUrl"]}api/accounts/{user.Id}?token={code}");
			
			return Ok();
		}
		
		[HttpPut]
		[Authorize(Roles = Roles.Company)]
		[ProducesResponseType(200)]
		[ProducesResponseType(400)]
		[Route("api/accounts/companies")]
		public async Task<IActionResult> Put(UpdateCompanyCommand command)
		{
			var user = await GetCurrentCompany();

			user.Name = command.Name;
			user.PhoneNumber = command.PhoneNumber;
			user.Address.Street = command.Street;
			user.Address.Neighborhood = command.Neighborhood;
			user.Address.City = command.City;
			user.Address.Uf = command.Uf;
			user.Address.Cep = command.Cep;
			
			return Ok();
		}
		
		// POST: api/Account/Company
		[HttpPut]
		[Authorize(Roles = "company, client")]
		[ProducesResponseType(200)]
		[ProducesResponseType(400)]
		[Route("api/accounts/password")]
		public async Task<IActionResult> Put(UpdateUserPasswordCommand command)
		{
			var user = await GetCurrentUser();

			var result = await _userManager.ChangePasswordAsync(user, command.CurrentPassword, command.Password);
			if(!result.Succeeded)
				AddMessage("CurrentPassword", "A sua senha atual está errada");

			if (!ModelState.IsValid)
				return Ok(ModelState);
			
			return Ok();
		}
		
		[HttpGet]
		[AllowAnonymous]
		[ProducesResponseType(200)]
		[ProducesResponseType(404)]
		[ProducesResponseType(400)]
		[Route("api/accounts/{userId}")]
		public async Task<IActionResult> Get(string userId, string token)
		{
			token = token.Replace("-", "/").Replace("_", "+");
			var user = await DbSession
				.Query<Company>()
				.SingleOrDefaultAsync(x => x.Id.Equals(userId));
			
			if (user == null)
				return NotFound();
			
			_logger.LogWarning(token);			
			
			var result = await _userManager.ConfirmEmailAsync(user, token);

			if (!result.Succeeded)
				return BadRequest(result.Errors);

			return Redirect($"{_configuration["FrontUrl"]}/login");
		}
	}
}