using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using TccBack.Domain.Entities;

namespace TccBack.Controllers
{
	[ApiController]
	public class ApplicationController : Controller
	{		
		protected IAsyncDocumentSession DbSession { get; set; }
		protected readonly IMapper Mapper;
		
		protected ApplicationController(IAsyncDocumentSession dbSession, IMapper mapper)
		{
			DbSession = dbSession ?? throw new ArgumentNullException(nameof(dbSession));
			Mapper = mapper;

			// RavenDB best practice: during save, wait for the indexes to update.
			// This way, Post-Redirect-Get scenarios won't be affected by stale indexes.
			// For more info, see https://ravendb.net/docs/article-page/3.5/Csharp/client-api/session/saving-changes
			DbSession.Advanced.WaitForIndexesAfterSaveChanges(timeout: TimeSpan.FromSeconds(30), throwOnTimeout: false);
		}

		public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
		{
			var executedContext = await next.Invoke();
			if (executedContext.Exception == null && ModelState.ErrorCount == 0)
			{
				await DbSession.SaveChangesAsync();
			}
		}
		
		protected void AddMessage(string key, string message)
		{
			ModelState.AddModelError(key, message);
		}

		protected async Task<ApplicationUser> GetCurrentUser()
		{
			var id = HttpContext.User.FindFirstValue(JwtRegisteredClaimNames.Sid);
			var client = await DbSession.Query<Client>()
				.SingleOrDefaultAsync(x => x.Id.Equals(id));
			
			if (client == null)
			{
				var company = await DbSession.Query<Company>()
					.SingleOrDefaultAsync(x => x.Id.Equals(id));

				return company;
			}

			return client;
		}

		protected async Task<Company> GetCurrentCompany()
		{
			var id = HttpContext.User.FindFirstValue(JwtRegisteredClaimNames.Sid);
			return await DbSession.Query<Company>()
				.SingleOrDefaultAsync(x => x.Id.Equals(id));
		}

		protected async Task<Client> GetCurrentClient()
		{
			var id = HttpContext.User.FindFirstValue(JwtRegisteredClaimNames.Sid);
			return await DbSession.Query<Client>()
				.SingleOrDefaultAsync(x => x.Id.Equals(id));
		}
	}
}