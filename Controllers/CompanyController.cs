using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using TccBack.Domain.Commands.CompanyCommands.Output;
using TccBack.Domain.Entities;

namespace TccBack.Controllers
{
	public class CompanyController : ApplicationController
	{
		public CompanyController(IAsyncDocumentSession dbSession, IMapper mapper) : base(dbSession, mapper)
		{
		}
		
		[HttpGet]
		[Authorize(Roles = "client, administrator")]
		[ProducesResponseType(200)]
		[ProducesResponseType(404)]
		[Route("api/companies/{id}")]
		public async Task<IActionResult> Get(string id)
		{
			var company = await DbSession.Query<Company>()
				.SingleOrDefaultAsync(x => x.Id.Equals(id));

			if (company == null)
				return NotFound();

			return Ok(Mapper.Map<Company, CompanyCommandResult>(company));
		}
	}
}