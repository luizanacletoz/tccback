using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Raven.Client.Documents;
using Raven.Client.Documents.Linq;
using Raven.Client.Documents.Session;
using TccBack.Domain.Commands.NotificationCommands.Output;
using TccBack.Domain.Entities;

namespace TccBack.Controllers
{
    public class NotificationController : ApplicationController
    {
        public NotificationController(IAsyncDocumentSession dbSession, IMapper mapper) : base(dbSession, mapper)
        {
        }

        [HttpGet]
        [Authorize(Roles = "client, company")]
        [Route("api/notifications")]
        public async Task<IActionResult> Get(bool getAmount)
        {
            var client = await GetCurrentClient();
            if (client != null)
            {
                var notifications = await DbSession.Query<ClientNotification>()
                    .Where(x => x.ClientId.Equals(client.Id))
                    .Where(x => !x.IsReaded)
                    .OrderByDescending(x => x.CreatedAt)
                    .ToListAsync();
                
                foreach (var companyNotification in notifications)
                {
                    companyNotification.IsReaded = true;
                }

				if (getAmount)
                    return Ok(new { Amount = notifications.Count });

                return Ok(
                    Mapper.Map<IList<ClientNotification>, IList<ClientNotificationCommandResult>>(notifications));
            }

            var company = await GetCurrentCompany();
            if (company != null)
            {
                var notifications = await DbSession.Query<CompanyNotification>()
                    .Where(x => x.CompanyId.Equals(company.Id))
                    .Where(x => !x.IsReaded)
                    .OrderByDescending(x => x.CreatedAt).ToListAsync();


                foreach (var companyNotification in notifications)
                {
                    companyNotification.IsReaded = true;
                }

                if (getAmount)
                    return Ok(new { Amount = notifications.Count });

                return Ok(
                    Mapper.Map<IList<CompanyNotification>, IList<CompanyNotificationCommandResult>>(notifications));
            }

            return NotFound();
        }



        [HttpPut]
        [Authorize(Roles = "client, company")]
        [Route("api/notifications/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var client = await GetCurrentClient();
            if (client != null)
                (await DbSession.LoadAsync<ClientNotification>(id)).Read();

            var company = await GetCurrentCompany();
            if (company != null)
                (await DbSession.LoadAsync<CompanyNotification>(id)).Read();

            return Ok();
        }
    }
}