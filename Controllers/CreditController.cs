using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Raven.Client.Documents.Session;
using TccBack.Domain.Commands.CreditCommands.Input;
using TccBack.Services.Payment.PaymentEntities;
using TccBack.Services.Payment;
using Payment = TccBack.Domain.Entities.Payment;

namespace TccBack.Controllers
{
	public class CreditController : ApplicationController
	{

		private readonly PaymentService _paymentService;
		
		public CreditController(
			IAsyncDocumentSession dbSession, 
			IMapper mapper, 
			PaymentService paymentService
			) : base(dbSession, mapper)
		{
			_paymentService = paymentService;
		}
		
		[HttpPost]
		[ProducesResponseType(typeof(PaymentResponse), 200)]
		[ProducesResponseType(typeof(PaymentResponse), 400)]
		[Authorize(Roles = "company")]
		[Route("api/credits")]
		public async Task<IActionResult> Post(AddCreditCommand command)
		{
			var company = await GetCurrentCompany();

			var paymentId = Guid.NewGuid().ToString();
			var simplePayment = new SimplePayment(
				paymentId, 
				PaymentType.CreditCard, 
				(int) command.Credits * 100,
				command.Installments, 
				command.CreditCard.CardNumber, 
				command.CreditCard.Holder,
				command.CreditCard.ExpirationDate,
				command.CreditCard.SecurityCode,
				command.CreditCard.Brand);
			
			var result = await _paymentService.SendSimplePayment(simplePayment);

			if (
				result.ReturnCode == (int) EPaymentCode.Authorized4 || 
				result.ReturnCode == (int) EPaymentCode.Authorized6)
			{
				var payment = new Payment(
					paymentId, 
					company.Id, 
					command.Credits, 
					result.PaymentId);

				company.AddCredits(command.Credits);

				await DbSession.StoreAsync(payment);
				
				return Ok(result); 
			}

			return BadRequest(result);
		}
		
	}
}