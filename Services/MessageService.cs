using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Google.Cloud.PubSub.V1;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Newtonsoft.Json;
using TccBack.Domain.Entities;
using TccBack.Services.PubSub;

namespace TccBack.Services
{
	public class MessageService : IEmailSender
	{

		private readonly ILogger<MessageService> _logger;
		private readonly IConfiguration _configuration;
		private PubSubService _pubSubService;
		
		public MessageService(
			ILogger<MessageService> logger, 
			IConfiguration configuration, 
			PubSubService pubSubService)
		{
			_logger = logger;
			_configuration = configuration;
			_pubSubService = pubSubService;
		}
		
		public async Task SendEmailAsync(string email, string subject, string content)
		{
			var message = new
			{
				From = new
				{
					Email = _configuration["EmailSender"],
					Name = "OrçaFácil"
				},
				To = new
				{
					Email = email
				},
				Subject = subject,
				Content = content,                
				TemplateId = _configuration["DefaultEmailTemplate"]

			};

			await _pubSubService.SendMessage(Topics.SendEmail, JsonConvert.SerializeObject(message));
		}
		
		public async Task SendEmailAsync(ApplicationUser to, string subject, string content)
		{
			var message = new
			{
				From = new
				{
					Email = _configuration["EmailSender"],
					Name = "OrçaFácil"
				},
				To = new
				{
					Email = to.Email,
					Name = to.Name	
				},
				Subject = subject,
				Content = content,
				Name = to.Name,
				TemplateId = _configuration["DefaultEmailTemplate"],
			};
			
			await _pubSubService.SendMessage(Topics.SendEmail, JsonConvert.SerializeObject(message));
		}
		
		public async Task SendActivationEmailAsync(ApplicationUser to, string subject, string content, string activationLink)
		{
			var message = new
			{
				From = new
				{
					Email = _configuration["EmailSender"],
					Name = "OrçaFácil"
				},
				To = new
				{
					Email = to.Email,
					Name = to.Name	
				},
				Subject = subject,
				Content = content,
				Name = to.Name,
				ActivationLink = activationLink,
				TemplateId = _configuration["ActivationEmailTemplate"],
			};
			
			await _pubSubService.SendMessage(Topics.SendEmail, JsonConvert.SerializeObject(message));
		}
	}
}