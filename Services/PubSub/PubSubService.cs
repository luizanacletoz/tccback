using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Microsoft.Extensions.Configuration;

namespace TccBack.Services.PubSub
{
	public class PubSubService
	{
		private readonly IConfiguration _configuration;
		private readonly PublisherServiceApiClient _publisher;
		private readonly string _projectId;
		

		public PubSubService(IConfiguration configuration)
		{
			_configuration = configuration;
			_publisher = PublisherServiceApiClient.Create();
			_projectId = _configuration["projectId"];
		}

		public async Task<Topic> CreateTopic(string name)
		{
			var topicName = new TopicName(_projectId, name);

			var topic = await _publisher.CreateTopicAsync(topicName);
			return topic;
		}

		public async Task SendMessage(string topic, string message)
		{
			var publishRequest = new PublishRequest()
			{
				Topic = Topics.ProjectName + topic,
				Messages =
				{
					new PubsubMessage()
					{
						Data = ByteString.CopyFromUtf8(message)
					}
				}
			};
			
			await _publisher.PublishAsync(publishRequest);
		}
	}
}