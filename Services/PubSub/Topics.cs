namespace TccBack.Services.PubSub
{
	public static class Topics
	{
		public static  readonly string ProjectName = "projects/tccproject-220008/topics/";
		
		public static readonly string SendEmail = "send-email";
	}
}