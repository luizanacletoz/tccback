using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TccBack.Services.Payment.PaymentEntities;

namespace TccBack.Services.Payment
{
	public class PaymentService
	{
		private const string Sales = "/1/sales/";
		
		private readonly IConfiguration _configuration;
		private readonly ILogger<PaymentService> _logger;
		
		public PaymentService(IConfiguration configuration, ILogger<PaymentService> logger)
		{
			_configuration = configuration;
			_logger = logger;
		}

		public async Task<PaymentResponse> SendSimplePayment(SimplePayment simplePayment)
		{
			using (HttpClient httpClient = new HttpClient())
			{						
				var apiUrl = $"{_configuration["Cielo:Request"]}{Sales}";
				
				httpClient.DefaultRequestHeaders.Add("MerchantId", _configuration["Cielo:MerchantId"]);
				httpClient.DefaultRequestHeaders.Add("MerchantKey", _configuration["Cielo:MerchantKey"]);
				
				using (HttpResponseMessage httpResponse = await httpClient.PostAsync(apiUrl, new StringContent(JsonConvert.SerializeObject(simplePayment), Encoding.UTF8, "application/json")))
				{
					if (httpResponse.IsSuccessStatusCode)
					{
						var result = JsonConvert.DeserializeObject<dynamic>(await httpResponse.Content.ReadAsStringAsync());
						return new PaymentResponse
						{
							PaymentId = result.Payment.PaymentId,
							ReturnCode = result.Payment.ReturnCode
						};
					}

					return new PaymentResponse
					{
						ReturnCode = (int) EPaymentCode.TimeOut
					};
				}
			}
		}
	}
}