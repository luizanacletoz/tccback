namespace TccBack.Services.Payment.PaymentEntities
{
	public enum EPaymentCode
	{
		Authorized4 = 4,
		Authorized6 = 6,
		NotAuthorized = 57,
		ExpiredCard = 05,
		BlockedCard = 78,
		TimeOut = 99,
		CanceledCard = 77,
		ProblemWithCard = 70
	}
}