namespace TccBack.Services.Payment.PaymentEntities
{
	public class CreditCard
	{
		public CreditCard(string cardNumber, string holder, string expirationDate, string securityCode, string brand)
		{
			CardNumber = cardNumber;
			Holder = holder;
			ExpirationDate = expirationDate;
			SecurityCode = securityCode;
			Brand = brand;
		}

		public string CardNumber { get; set; }
		public string Holder { get; set; }
		public string ExpirationDate { get; set; }
		public string SecurityCode { get; set; }
		public string Brand { get; set; }
	}
}