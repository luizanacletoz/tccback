namespace TccBack.Services.Payment.PaymentEntities
{
	public class Payment
	{
		public Payment(string type, int amount, int installments, CreditCard creditCard)
		{
			Type = type;
			Amount = amount;
			Installments = installments;
			CreditCard = creditCard;
		}
		
		public string Type { get; set; }
		public int Amount { get; set; }
		public int Installments { get; set; }
		public CreditCard CreditCard { get; set; }
		
	}
}