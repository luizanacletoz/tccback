namespace TccBack.Services.Payment.PaymentEntities
{
	public class PaymentResponse
	{
		public string PaymentId { get; set; }
		public int ReturnCode { get; set; }
	}
}
