namespace TccBack.Services.Payment.PaymentEntities
{
	public class SimplePayment
	{
		public SimplePayment(
			string merchantOrderId, 
			string type, 
			int amount, 
			int installments, 
			string cardNumber, 
			string holder, 
			string expirationDate, 
			string securityCode, 
			string brand)
		{
			MerchantOrderId = merchantOrderId;
			
			var creditCard = new CreditCard(cardNumber, holder, expirationDate, securityCode, brand);
			Payment = new PaymentEntities.Payment(type, amount, installments, creditCard);
		}

		public string MerchantOrderId { get; set; }
		public PaymentEntities.Payment Payment { get; set; }
	}
}


