﻿using System;
using System.Globalization;
using System.IO.Compression;
using System.Text;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using Raven.Client.Documents;
using Raven.Identity;
using Swashbuckle.AspNetCore.Swagger;
using TccBack.Domain.AutoMapper;
using TccBack.Domain.Entities;
using TccBack.Services;
using TccBack.Services.Payment;
using TccBack.Services.PubSub;

namespace TccBack
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			AutoMapperConfig.RegisterMappings();
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			
			
			#region Services

			services.AddScoped<JwtService, JwtService>();
			services.AddScoped<PaymentService, PaymentService>();
			services.AddScoped<PubSubService, PubSubService>();

			services.AddScoped<IEmailSender, MessageService>();
			services.AddScoped<MessageService, MessageService>();
			
			#region NativeServices

			var docStore = new DocumentStore{
				Urls = new string[] { Configuration["RavenDb"] },
				Database = "OrcaFacil"
			}.Initialize();

			services
				.AddRavenDbAsyncSession(docStore)
				.AddRavenDbIdentity<ApplicationUser>(options =>
				{
					options.Password.RequireDigit = false;
					options.Password.RequiredLength = 5;
					options.Password.RequireLowercase = false;
					options.Password.RequireUppercase = false;
					options.Password.RequireNonAlphanumeric = false;

				});

			services.AddAuthorization(auth =>
			{
				auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
					.AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
					.RequireAuthenticatedUser().Build());
			});

			services.AddAuthentication(options =>
			{
				options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			}).AddJwtBearer(cfg =>
			{
				cfg.RequireHttpsMetadata = false;
				cfg.SaveToken = true;
				
				cfg.TokenValidationParameters = new TokenValidationParameters()
				{
					ValidIssuer = Configuration["Token:Issuer"],
					ValidAudience =  Configuration["Token:Issuer"],
					IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Token:Key"])),
					ClockSkew = TimeSpan.Zero
				};
			});
			
			services.AddResponseCompression().Configure<GzipCompressionProviderOptions>(options =>
			{
				options.Level = CompressionLevel.Optimal;
			});

			services.AddAutoMapper();
			
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info { Title = "TCC Api", Version = "v1" });
			});
			
			services.AddMvc()
				.AddJsonOptions(options =>
				{
					options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
				})
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
				.AddFluentValidation(fv =>
				{
					fv.RunDefaultMvcValidationAfterFluentValidationExecutes = true;
					fv.RegisterValidatorsFromAssemblyContaining<Startup>();
				});
            
            

			#endregion

			#endregion


			services.AddLogging();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{

			app.UseCors(builder => builder
				.AllowAnyOrigin()
				.AllowAnyMethod()
				.AllowAnyHeader()
				.AllowCredentials());   
				

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}
			
			app.UseSwagger();

			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "TCC Api V1");
			});

			
			app.UseAuthentication();
			app.UseResponseCompression();
			app.UseHttpsRedirection();
			app.UseMvc();
		}
	}
}